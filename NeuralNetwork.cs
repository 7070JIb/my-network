﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace Neural_Network_1
{
    public class NeuralNetwork
    {
        private double[,,] weights;
        private int[] layers;
        private double[,] outputs;
        private double[,] differences;
        public NNStatus nnStatus;
        public float learningPercent;

        public NeuralNetwork(int[] layers_)
        {
            layers = layers_;
            Random rand = new Random();

            nnStatus = NNStatus.NotActive;
            learningPercent = 0;

            //Создаются рандомные веса
            weights = new double[layers.Length - 1, layers.Max(), layers.Max()];
            for (int i = 0; i < layers.Length - 1; i++)
            {
                for (int j = 0; j < layers[i]; j++)
                {
                    for (int k = 0; k < layers[i + 1]; k++)
                    {
                        weights[i, j, k] = rand.NextDouble() * 2 - 1;
                    }
                }
            }
        }
        public NeuralNetwork(int[]layers_, double[,,] weights_)
        {
            nnStatus = NNStatus.NotActive;
            learningPercent = 0;

            layers = layers_;

            // Создание сети по существующим весам
            weights = weights_;
        }
        public double[] FeedForward(double[] input)
        { 
            nnStatus = NNStatus.Calculating;
            //Проверка на соответствие размера входящих данных размеру слоя входных данных
            if (input.Length != layers[0]) throw new ArgumentException();

            outputs = new double[layers.Length, layers.Max()];
            //заполнение выходов входного слоя входными данными
            for (int i = 0; i < layers[0]; i++)
            {
                outputs[0, i] = input[i];
            }

            //расчет результата, i - цикл уровня набора слоев
            for (int i = 0; i < layers.Length - 1; i++)
            {
                //k - уровень: слой с нейроном, для которого расчитывается выход
                for (int k = 0; k < layers[i + 1]; k++)
                {
                    //j - уровень: предыдущий слой, у которого суммируются выходы и веса связей со слоем k
                    double sum = 0;
                    for (int j = 0; j < layers[i]; j++)
                    {
                        sum += weights[i, j, k] * outputs[i, j];
                    }

                    //Пропуск через функцию активации
                    outputs[i + 1, k] = Sigm(sum);
                }
            }
            //получение результатов
            double[] result = new double[layers[layers.Length - 1]];
            for (int i = 0; i < layers[layers.Length - 1]; i++)
            {
                result[i] = outputs[layers.Length - 1, i];
            }

            nnStatus = NNStatus.NotActive;
            return result;
        }
        private double Sigm(double x)
        {
            return (1 / (1 + Math.Exp(-x)));
        }
        public void Learn(List<Tuple<List<double>, List<double>>> dataset_input_expected, int epochs, double learningRate)
        {
            learningPercent = 0;
            float learningValue = 0;
            float learningMaxValue = dataset_input_expected.Count * epochs;
                    
            for (int i = 0; i < epochs; i++)
            {
                for(int j =0; j < dataset_input_expected.Count; j++)
                {
                    Backpropogation(dataset_input_expected[j].Item1, dataset_input_expected[j].Item2, learningRate);
                    learningValue++;
                    learningPercent = (learningValue / learningMaxValue) * 100f;                  
                }
            }
            nnStatus = NNStatus.NotActive;
        }
        private void Backpropogation(List<double> inputs, List<double> expected, double learningRate)
        {
            differences = new double[layers.Length, layers.Max()];

            //Сначала находим ошибку выходного нейрона
            double[] actual = FeedForward(inputs.ToArray());
            for (int i = 0; i < expected.Count; i++)
            {
                differences[layers.Length - 1, i] = expected[i] - actual[i];
            }
           
            //Находим ошибки остальных нейронов,начиная с предпоследнего слоя            
            for (int i = layers.Length - 2; i > 0; i--)
            {
                for (int j = 0; j < layers[i]; j++)
                {
                    differences[i, j] = 0;
                    for (int k = 0; k < layers[i + 1]; k++)
                    {
                        differences[i, j] += differences[i + 1, k] * weights[i, j, k];
                    }
                }

            }

            //Корректируем веса
            for (int i = 0; i < layers.Length - 1; i++)
            {
                for (int j = 0; j < layers[i]; j++)
                {
                    for (int k = 0; k < layers[i + 1]; k++)
                    {
                        //вес += выход выход левого нейрона * производная акт функции от выхода правого нейрона *
                        //* коэф обучения * ошибка правого нейрона
                        weights[i, j, k] += outputs[i, j] * (outputs[i + 1, k] * (1 - outputs[i + 1, k])) * learningRate * differences[i + 1, k];
                    }
                }
            }
        }
        public void SaveModel(string path = "model")
        {
            nnStatus = NNStatus.Saving;
            using (FileStream fileStream = new FileStream("model", FileMode.Create, FileAccess.Write))
            {
                BinaryWriter writer = new BinaryWriter(fileStream);
                writer.Write(layers.Length);
                for(int i = 0; i < layers.Length;i++)
                {
                    writer.Write(layers[i]);
                }
                for(int i = 0; i < layers.Length - 1; i++)
                {
                    for(int j = 0; j < layers[i];j++)
                    {
                        for(int k = 0; k < layers[i+1];k++)
                        {
                            writer.Write(weights[i, j, k]);
                        }                       
                    }                    
                }
            }
            nnStatus = NNStatus.NotActive;
        }
        public static NeuralNetwork LoadModel(string path = "model")
        {     
            using (FileStream fileStream = new FileStream("model", FileMode.Open, FileAccess.Read))
            {
                BinaryReader reader = new BinaryReader(fileStream);

                int layersLength = reader.ReadInt32();

                int[] layers_ = new int[layersLength];
                for (int i = 0; i < layersLength; i++)
                {
                    layers_[i] = reader.ReadInt32();
                }

                double[,,] weights_ = new double[layers_.Length - 1, layers_.Max(), layers_.Max()];
                for (int i = 0; i < layers_.Length - 1; i++)
                {
                    for (int j = 0; j < layers_[i]; j++)
                    {
                        for (int k = 0; k < layers_[i + 1]; k++)
                        {
                            weights_[i, j, k] = reader.ReadDouble();
                        }
                    }
                }

                return new NeuralNetwork(layers_, weights_);
            }
        }
        public static List<Tuple<List<double>, List<double>>> ParseMoveDataTSV(string filePath, params int[] resultColumnsNumbers)
        {
            List<Tuple<List<double>, List<double>>> dataset = new List<Tuple<List<double>, List<double>>>();
            using (StreamReader reader = new StreamReader(filePath))
            {
                string[] columnNames = reader.ReadLine().Split('\t').TakeWhile(t => t != string.Empty).ToArray();

                string str = reader.ReadLine();
                while (str != null)
                {
                    string[] dataraw = str.Split('\t');

                    double[] dataparsed = new double[dataraw.Length];

                    for (int i = 0; i < dataraw.Length; i++)
                        dataparsed[i] = double.Parse(dataraw[i]);

                    List<double> featuresList = new List<double>();
                    List<double> result = new List<double>();
                    for (int i = 0; i < dataparsed.Length; i++)
                    {
                        if (resultColumnsNumbers.Contains(i))
                        {
                            result.Add(dataparsed[i]);
                        }
                        else
                        {
                            featuresList.Add(dataparsed[i]);
                        }
                    }

                    dataset.Add(new Tuple<List<double>, List<double>>(featuresList, result));
                    str = reader.ReadLine();
                }
            }
            return dataset;
        }

        public enum NNStatus
        {
            Learning,
            Calculating,
            Saving,
            NotActive
        }
    }
}
